from reportlab.pdfgen import canvas
import reportlab.lib.pagesizes
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl
from PIL import Image
import os
import pyqrcode
import shutil
import pyperclip

A4 = reportlab.lib.pagesizes.A4

# need to install below:
# pip install pdfrw
# pip install ReportLab
# pip install PyQRCode
# pip install pypng
# pip install pyperclip


# this data is string
user_input = input('prefix?: ')
prefix=user_input

user_input = input('Enter qty: ')
num_to_add=int(user_input)

user_input = input('from?: ')
init_num=int(user_input)

user_input = input('start from which row?: ')
cnt_y=int(user_input)-1

margin_x=104
margin_y=31 #35is too big
cnt_x=0
serialID_list=""

os.makedirs('img', exist_ok=True)

# generate pdf
c = canvas.Canvas("test.pdf", pagesize=A4)

c.setFont('Helvetica', 7)#this is default font. can't be empty first argument

for i in range(init_num, init_num+num_to_add):
    if cnt_x>=5 and cnt_x%5==0:
        cnt_y+=1
        cnt_x=0

    serialID=prefix+f'{i:05}'
    serialID_list=serialID_list+serialID+'\n'

    # generate qr code and draw
    code = pyqrcode.create(serialID, error='L', version=3, mode='binary')
    qr_file_path="img/"+serialID+".png"# path or file name?
    code.png(qr_file_path, scale=3, module_color=[0, 0, 0, 128], background=[255, 255, 255])
    img = Image.open(qr_file_path)
    c.drawInlineImage(img,60+margin_x*cnt_x, A4[1] - 55-margin_y*cnt_y, 20, 20)

    #draw letters
    c.drawString(86+margin_x*cnt_x, A4[1] - 50-margin_y*cnt_y, serialID)
    cnt_x+=1


c.rect(0, 0, A4[0], A4[1])
c.showPage()
c.save()

#output serialIDs to text file
f = open(prefix+'.txt', 'w')#w mode means: if no such file->create the file,if already exist the file->overwrite to the file
f.write(serialID_list)

#copy to clipboard
pyperclip.copy(serialID_list)

# delete directory incl files inside
# shutil.rmtree('img/')